import { registerDependencies } from 'mjml-validator'
import { BodyComponent } from 'mjml-core'

registerDependencies({
  // Tell the validator which tags are allowed as our component's parent
  'mj-body': ['mj-explore-more'],
  'mj-column': ['mj-explore-more'],
  'mj-table': ['mj-explore-more'],
  // Tell the validator which tags are allowed as our component's children
  'mj-explore-more': [
    'mj-table',
    'mj-body',
    'mj-section',
  ],
})

/*
  Our component is a (useless) simple text tag, that adds colored stars around the text.
  It can take 3 attributes, to specify size and colors.
*/
export default class MjExploreMore extends BodyComponent {
  // Tell the parser that our component won't contain other mjml tags
  static endingTag = true

  // Tells the validator which attributes are allowed for mj-layout
  static allowedAttributes = {
    'link-href': 'string',
  }

  // What the name suggests. Fallback value for this.getAttribute('attribute-name').
  static defaultAttributes = {
    'link-href': 'null ',
  }


  /*
    Render is the only required function in a component.
    It must return an html string.
  */
  render() {
    return `
    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%"
    style="background-color: #FFFFFF;">
    <tr>
      <td>
        <table width="100%" align="center" bgcolor="ffffff" cellspacing="0"
          cellpadding="0" border="0" style="padding: 0 20px; max-width: 560px; margin: auto;">
          <tr>
            <td valign="top" style="text-align:center;" bgcolor="ffffff">
              <table width="100%" border="0" cellspacing="0" cellpadding="0"
              style="display:block;" bgcolor="f1f2f2">
              <tr>
                <td width="100%" align="left" valign="middle" style="padding:20px 10px 20px 30px;
                width:100%;">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="88%" align="left" valign="top" style="font-family:
                      Century Gothic, CenturyGothic, AppleGothic Regular, Apple Gothic Regular,
                      Arial, sans-serif; font-weight:bold; font-size: 21px; line-height: 28px;
                      color:#5514B4; mso-line-height-rule:exactly;width:100%;"
                        class="subhead2">
                        <a
                        ${this.htmlAttributes({
                          href: this.getAttribute('link-href'),
                        })}
                        style="color:#5514B4; text-decoration:none !important; width:100%;">
                        ${this.getContent()}
                        </a></td>
                      <td width="2%">&nbsp;</td>
                      <td width="10%" align="right">
                        <a
                        ${this.htmlAttributes({
                          href: this.getAttribute('link-href'),
                        })}
                        ><img class="item--icon" src="purplearrowright.gif" width="13" height="22"
                        alt="arrow icon" border="0" style="display:block;" /></a>
                      </td>
                      <td width="2%">&nbsp;</td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td height="5" bgcolor="ffffff"></td>
              </tr>
            </table>

              </td>
            </tr>
          </table>
        </td>
      </tr>
      </table>
		`
  }
}
