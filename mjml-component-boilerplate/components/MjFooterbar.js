import { registerDependencies } from 'mjml-validator'
import { BodyComponent } from 'mjml-core'

registerDependencies({
  'mj-footerbar': [],
  'mj-body': ['mj-footerbar'],
  'mj-wrapper': ['mj-footerbar'],
  'mj-column': ['mj-footerbar'],
})

export default class MjFooterbar extends BodyComponent {
  // Tell the parser that our component won't contain other mjml tags
  static endingTag = false

  // Tells the validator which attributes are allowed for mj-layout
  static allowedAttributes = {
    'mybt-href': 'string',
    'help-href': 'string',
    'unsubscribe-href': 'string',
    'twitter-href': 'string',
    'facebook-href': 'string',
    'youtube-href': 'string',
  }

  // What the name suggests. Fallback value for this.getAttribute('attribute-name').
  static defaultAttributes = {
    'mybt-href': 'null ',
    'help-href': 'null ',
    'unsubscribe-href': 'null ',
    'twitter-href': 'null ',
    'facebook-href': 'null ',
    'youtube-href': 'null ',
  }

   /*
     Render is the only required function in a component.
     It must return an html string.
   */
  render() {
    return `
    <table style="background-color: #5514B4; max-width: 600px;" width="600"
    role="presentation" cellspacing="0" cellpadding="0" border="0">
    <tbody>
    <tr>
      <td width="2%" style="background-color: #5514B4;" class="footergutter"></td>
      <td width="14%">
        <a
        ${this.htmlAttributes({
          href: this.getAttribute('mybt-href'),
        })}
        style="color:#FFFFFF;font-size: 16px; line-height: 18px; font-family: Century Gothic,
        CenturyGothic, AppleGothic Regular, Apple Gothic Regular, Arial, sans-serif;
        font-weight: bold; padding-left: 5px; text-decoration:none!important;"
        class="footertext fallback-font">
        MyBT
        </a>
      </td>
      <td width="12%">
        <a
        ${this.htmlAttributes({
          href: this.getAttribute('help-href'),
        })}
        style="color:#FFFFFF;font-size: 16px; line-height: 18px; font-family: Century Gothic,
        CenturyGothic, AppleGothic Regular, Apple Gothic Regular, Arial, sans-serif;
        font-weight: bold;text-decoration:none!important;" class="footertext fallback-font">
        Help</a>
      </td>
      <td width="28%">
        <a
        ${this.htmlAttributes({
          href: this.getAttribute('unsubscribe-href'),
        })}
        title="Unsubscribe" style="color:#FFFFFF;font-size: 16px; line-height: 18px; font-family:
        Century Gothic, CenturyGothic, AppleGothic Regular, Apple Gothic Regular, Arial,
        sans-serif;
        font-weight: bold;text-decoration:none!important;" class="footertext fallback-font">
        Unsubscribe</a>
      </td>
      <td width="12%" class="footerless"></td>
      <td width="10%" align="right" class="footerwidthless">
        <a
        ${this.htmlAttributes({
          href: this.getAttribute('twitter-href'),
        })}
        ><img src="twitter.gif" alt="BT Twitter" style="display: block;" height="25" width="25"
        class="footericon"></a>
      </td>
      <td width="10%" align="right" class="footerwidthless">
        <a
        ${this.htmlAttributes({
          href: this.getAttribute('facebook-href'),
        })}
        ><img src="facebook.gif" alt="BT Facebook" style="display: block;" height="25" width="25"
        class="footericon"></a>
      </td>
      <td width="10%" align="right" class="footerwidthless" style="">
        <a
        ${this.htmlAttributes({
          href: this.getAttribute('youtube-href'),
        })}
        ><img src="youtube.gif" alt="BT youtube" style="display: block;" height="25" width="25"
        class="footericon"></a>
      </td>
      <td width="2%" style="background-color: #5514B4;" class="footergutter"></td>
    </tr>
  </tbody>
  </table>
     `
  }
 }
