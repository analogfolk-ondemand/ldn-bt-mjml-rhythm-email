import { registerDependencies } from 'mjml-validator'
import { BodyComponent } from 'mjml-core'

registerDependencies({
  // Tell the validator which tags are allowed as our component's parent
  'mj-body': ['mj-bar-footer-sport'],
  'mj-column': ['mj-bar-footer-sport'],
  // Tell the validator which tags are allowed as our component's children
  'mj-bar-footer-sport': [],
})

export default class MjBarFooterSport extends BodyComponent {
  // Tell the parser that our component won't contain other mjml tags
  static endingTag = false

  // Tells the validator which attributes are allowed for mj-layout
  static allowedAttributes = {
    'mybt-href': 'string',
    'help-href': 'string',
    'unsubscribe-href': 'string',
    'twitter-href': 'string',
    'facebook-href': 'string',
    'youtube-href': 'string',
  }

  // What the name suggests. Fallback value for this.getAttribute('attribute-name').
  static defaultAttributes = {
    'mybt-href': 'null ',
    'help-href': 'null ',
    'unsubscribe-href': 'null ',
    'twitter-href': 'null ',
    'facebook-href': 'null ',
    'youtube-href': 'null ',
  }

  /*
    Render is the only required function in a component.
    It must return an html string.

    TODO should really have mj-section and mj-column in here not the HTML itself
  */
  render() {
    return `
    <!--[if mso | IE]>
    <table align="center" role="presentation" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" style="vertical-align:top;width:600px;">
    <![endif]-->
    <div style="background:#5514b4;background-color:#5514b4;margin:0px auto;max-width:600px;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
      style="background:#5514b4;background-color:#5514b4;width:100%;">
        <tbody>
          <tr>
            <td style="direction:ltr;padding:20px 0;text-align:center;">
              <div class="mj-column-per-100 mj-outlook-group-fix" style="
              text-align:left; direction:ltr;display:inline-block;vertical-align:top;width:100%;">

          <table style="background-color: #5514B4; max-width: 600px;" width="100%"
          role="presentation" cellspacing="0" cellpadding="0" border="0">
          <tbody>
          <tr>
              <td width="2%" style="background-color: #5514B4;" class="footergutter">&nbsp;</td>
              <td width="14%">
                <a
                ${this.htmlAttributes({
                  href: this.getAttribute('mybt-href'),
                })}
                style="color:#FFFFFF;font-size: 16px; line-height: 18px; font-family:
                Century Gothic, CenturyGothic, AppleGothic Regular, Apple Gothic Regular,
                Arial, sans-serif; font-weight: bold; text-decoration:none!important;"
                class="footertext fallback-font">
                MyBT
                </a>
              </td>
              <td width="12%">
                <a
                ${this.htmlAttributes({
                  href: this.getAttribute('help-href'),
                })}
                style="color:#FFFFFF;font-size: 16px; line-height: 18px; font-family:
                Century Gothic, CenturyGothic, AppleGothic Regular, Apple Gothic Regular,
                Arial, sans-serif; font-weight: bold;text-decoration:none!important;"
                class="footertext fallback-font">
                Help</a>
              </td>
              <td width="14%" class="footerless">&nbsp;</td>
              <td width="10%" align="right" class="footerwidthless">
                <a
                ${this.htmlAttributes({
                  href: this.getAttribute('twitter-href'),
                })}
                ><img src="twitter.gif" alt="BT Twitter" style="display: block;" height="25"
                width="25" class="footericon"></a>
              </td>
              <td width="10%" align="right" class="footerwidthless">
                <a
                ${this.htmlAttributes({
                  href: this.getAttribute('facebook-href'),
                })}
                ><img src="facebook.gif" alt="BT Facebook" style="display: block;" height="25"
                width="25" class="footericon"></a>
              </td>
              <td width="10%" align="right" class="footerwidthless" style="">
                <a
                ${this.htmlAttributes({
                  href: this.getAttribute('youtube-href'),
                })}
                ><img src="youtube.gif" alt="BT youtube" style="display: block;" height="25"
                width="25" class="footericon"></a>
              </td>
              <td width="2%" style="background-color: #5514B4;" class="footergutter">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="8">&nbsp;</td>
            </tr>
            <tr>
              <td width="2%" style="background-color: #5514B4;" class="footergutter">&nbsp;</td>
              <td width="80%" colspan="6" class="footer-width">
                <a
                ${this.htmlAttributes({
                  href: this.getAttribute('unsubscribe-href'),
                })}
                 title="Unsubscribe" style="color:#FFFFFF;font-size: 16px; line-height: 18px;
                 font-family: Century Gothic, CenturyGothic, AppleGothic Regular,
                 Apple Gothic Regular, Arial, sans-serif; font-weight: bold;
                 text-decoration:none!important;" class="footertext fallback-font">
                 Unsubscribe from BT Sport newsletters</a>
                </td>
              <td width="2%" style="background-color: #5514B4;" class="footergutter">&nbsp;</td>
            </tr>
          </tbody>
          </table>
        </td>
      </tr>
    </tbody>
    </table>
    </td>
    </tr>
    </table>
    </div>
    <!--[if mso | IE]>
      </td>
    </tr>
  </table>
  <![endif]-->
		`
  }
}
