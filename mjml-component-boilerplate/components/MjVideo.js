import { registerDependencies } from 'mjml-validator'
import { BodyComponent } from 'mjml-core'

registerDependencies({
  // Tell the validator which tags are allowed as our component's parent
  'mj-column': ['mj-video'],
  // Tell the validator which tags are allowed as our component's children
  'mj-video': [],
})

export default class MjVideo extends BodyComponent {
  // Tell the parser that our component won't contain other mjml tags
  static endingTag = false

  // Tells the validator which attributes are allowed for mj-layout
  static allowedAttributes = {
    'src': 'string',
    'mp4-src': 'string',
    'webm-src': 'string',
    'poster-img': 'string',
    'fallback-img': 'string',
    'fallback-href': 'string',
    'fallback-alt': 'string',
  }

  // What the name suggests. Fallback value for this.getAttribute('attribute-name').
  static defaultAttributes = {
    'mp4-src': 'null ',
    'webm-src': 'null',
    'poster-img': 'https://via.placeholder.com/600x338.png?text=VIDEO+POSTER',
    'fallback-href': 'string',
    'fallback-img': 'https://via.placeholder.com/600x338.png?text=VIDEO+FALLBACK',
    'fallback-alt': 'null',
  }

  /*
    Render is the only required function in a component.
    It must return an html string.
  */
  render() {
    return `
      <div class="video">
      <video autoplay='autoplay' muted='muted' controls='true' preload='metadata'
      ${this.htmlAttributes({
        poster: this.getAttribute('poster-img'),
      })}
      >
        <source
        ${this.htmlAttributes({
          src: this.getAttribute('mp4-src'),
        })}
        type='video/mp4'
        />
        <source
        ${this.htmlAttributes({
          src: this.getAttribute('webm-src'),
        })}
        type='video/webm'
        />

      <a
        ${this.htmlAttributes({
          href: this.getAttribute('fallback-href'),
        })}>
        <img
        ${this.htmlAttributes({
          src: this.getAttribute('fallback-img'),
        })}
        ${this.htmlAttributes({
          alt: this.getAttribute('fallback-alt'),
        })}
        style="display: block; height:auto; border:none; width: 100%;max-width: 600px; height: auto;" width="600" class="g-img video-fallback">
      </a>
      </video>
      </div>
		`
  }
}
