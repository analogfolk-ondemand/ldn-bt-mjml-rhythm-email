'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp;

var _mjmlValidator = require('mjml-validator');

var _mjmlCore = require('mjml-core');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

(0, _mjmlValidator.registerDependencies)({
  // Tell the validator which tags are allowed as our component's parent
  'mj-column': ['mj-video'],
  // Tell the validator which tags are allowed as our component's children
  'mj-video': []
});

var MjVideo = (_temp = _class = function (_BodyComponent) {
  _inherits(MjVideo, _BodyComponent);

  function MjVideo() {
    _classCallCheck(this, MjVideo);

    return _possibleConstructorReturn(this, (MjVideo.__proto__ || Object.getPrototypeOf(MjVideo)).apply(this, arguments));
  }

  _createClass(MjVideo, [{
    key: 'render',


    /*
      Render is the only required function in a component.
      It must return an html string.
    */


    // Tells the validator which attributes are allowed for mj-layout
    value: function render() {
      return '\n      <div class="video">\n      <video autoplay=\'autoplay\' muted=\'muted\' controls=\'true\' preload=\'metadata\'\n      ' + this.htmlAttributes({
        poster: this.getAttribute('poster-img')
      }) + '\n      >\n        <source\n        ' + this.htmlAttributes({
        src: this.getAttribute('mp4-src')
      }) + '\n        type=\'video/mp4\'\n        />\n        <source\n        ' + this.htmlAttributes({
        src: this.getAttribute('webm-src')
      }) + '\n        type=\'video/webm\'\n        />\n\n      <a\n        ' + this.htmlAttributes({
        href: this.getAttribute('fallback-href')
      }) + '>\n        <img\n        ' + this.htmlAttributes({
        src: this.getAttribute('fallback-img')
      }) + '\n        ' + this.htmlAttributes({
        alt: this.getAttribute('fallback-alt')
      }) + '\n        style="display: block; height:auto; border:none; width: 100%;max-width: 600px; height: auto;" width="600" class="g-img video-fallback">\n      </a>\n      </video>\n      </div>\n\t\t';
    }

    // What the name suggests. Fallback value for this.getAttribute('attribute-name').

    // Tell the parser that our component won't contain other mjml tags

  }]);

  return MjVideo;
}(_mjmlCore.BodyComponent), _class.endingTag = false, _class.allowedAttributes = {
  'src': 'string',
  'mp4-src': 'string',
  'webm-src': 'string',
  'poster-img': 'string',
  'fallback-img': 'string',
  'fallback-href': 'string',
  'fallback-alt': 'string' }, _class.defaultAttributes = {
  'mp4-src': 'null ',
  'webm-src': 'null',
  'poster-img': 'https://via.placeholder.com/600x338.png?text=VIDEO+POSTER',
  'fallback-href': 'string',
  'fallback-img': 'https://via.placeholder.com/600x338.png?text=VIDEO+FALLBACK',
  'fallback-alt': 'null' }, _temp);
exports.default = MjVideo;