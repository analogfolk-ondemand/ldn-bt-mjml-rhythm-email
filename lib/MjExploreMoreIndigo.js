'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp;

var _mjmlValidator = require('mjml-validator');

var _mjmlCore = require('mjml-core');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

(0, _mjmlValidator.registerDependencies)({
  // Tell the validator which tags are allowed as our component's parent
  'mj-body': ['mj-explore-more-indigo'],
  'mj-column': ['mj-explore-more-indigo'],
  'mj-table': ['mj-explore-more-indigo'],
  // Tell the validator which tags are allowed as our component's children
  'mj-explore-more-indigo': ['mj-table', 'mj-body', 'mj-section']
});

/*
  Our component is a (useless) simple text tag, that adds colored stars around the text.
  It can take 3 attributes, to specify size and colors.
*/
var MjExploreMoreIndigo = (_temp = _class = function (_BodyComponent) {
  _inherits(MjExploreMoreIndigo, _BodyComponent);

  function MjExploreMoreIndigo() {
    _classCallCheck(this, MjExploreMoreIndigo);

    return _possibleConstructorReturn(this, (MjExploreMoreIndigo.__proto__ || Object.getPrototypeOf(MjExploreMoreIndigo)).apply(this, arguments));
  }

  _createClass(MjExploreMoreIndigo, [{
    key: 'render',


    /*
      Render is the only required function in a component.
      It must return an html string.
    */


    // Tells the validator which attributes are allowed for mj-layout
    value: function render() {
      return '\n    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%"\n    style="background-color: #5514B4;">\n    <tr>\n      <td>\n        <table width="100%" align="center" bgcolor="ffffff" cellspacing="0"\n          cellpadding="0" border="0" style="padding: 0 20px; max-width: 560px; margin: auto;">\n          <tr>\n            <td valign="top" style="text-align:center;" bgcolor="ffffff">\n              <table width="100%" border="0" cellspacing="0" cellpadding="0"\n              style="display:block;" bgcolor="f1f2f2">\n              <tr>\n                <td width="100%" align="left" valign="middle" style="padding:20px 10px 20px 30px;\n                width:100%;">\n                  <table width="100%" border="0" cellspacing="0" cellpadding="0">\n                    <tr>\n                      <td width="88%" align="left" valign="top" style="font-family:\n                      Century Gothic, CenturyGothic, CentGothWGL, -apple-system, BlinkMacSystemFont,\n                      Arial, Tahoma, Geneva, sans-serif; font-weight:bold; font-size: 21px; line-height: 28px;\n                      color:#5514B4; mso-line-height-rule:exactly;width:100%;"\n                        class="subhead2">\n                        <a\n                        ' + this.htmlAttributes({
        href: this.getAttribute('link-href')
      }) + '\n                        style="color:#5514B4; text-decoration:none !important; width:100%;">\n                        ' + this.getContent() + '\n                        </a></td>\n                      <td width="2%">&nbsp;</td>\n                      <td width="10%" align="right">\n                        <a\n                        ' + this.htmlAttributes({
        href: this.getAttribute('link-href')
      }) + '\n                        ><img class="item--icon" src="purplearrowright.gif" width="13" height="22"\n                        alt="arrow icon" border="0" style="display:block;" /></a>\n                      </td>\n                      <td width="2%">&nbsp;</td>\n                    </tr>\n                  </table>\n                </td>\n              </tr>\n              <tr>\n                <td height="5" bgcolor="5514B4"></td>\n              </tr>\n            </table>\n\n              </td>\n            </tr>\n          </table>\n        </td>\n      </tr>\n      </table>\n\t\t';
    }

    // What the name suggests. Fallback value for this.getAttribute('attribute-name').

    // Tell the parser that our component won't contain other mjml tags

  }]);

  return MjExploreMoreIndigo;
}(_mjmlCore.BodyComponent), _class.endingTag = true, _class.allowedAttributes = {
  'link-href': 'string' }, _class.defaultAttributes = {
  'link-href': 'null ' }, _temp);
exports.default = MjExploreMoreIndigo;