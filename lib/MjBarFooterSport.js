'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp;

var _mjmlValidator = require('mjml-validator');

var _mjmlCore = require('mjml-core');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

(0, _mjmlValidator.registerDependencies)({
  // Tell the validator which tags are allowed as our component's parent
  'mj-body': ['mj-bar-footer-sport'],
  'mj-column': ['mj-bar-footer-sport'],
  // Tell the validator which tags are allowed as our component's children
  'mj-bar-footer-sport': []
});

var MjBarFooterSport = (_temp = _class = function (_BodyComponent) {
  _inherits(MjBarFooterSport, _BodyComponent);

  function MjBarFooterSport() {
    _classCallCheck(this, MjBarFooterSport);

    return _possibleConstructorReturn(this, (MjBarFooterSport.__proto__ || Object.getPrototypeOf(MjBarFooterSport)).apply(this, arguments));
  }

  _createClass(MjBarFooterSport, [{
    key: 'render',


    /*
      Render is the only required function in a component.
      It must return an html string.
       TODO should really have mj-section and mj-column in here not the HTML itself
    */


    // Tells the validator which attributes are allowed for mj-layout
    value: function render() {
      return '\n    <!--[if mso | IE]>\n    <table align="center" role="presentation" border="0" cellpadding="0" cellspacing="0">\n      <tr>\n        <td align="center" style="vertical-align:top;width:600px;">\n    <![endif]-->\n    <div style="background:#5514b4;background-color:#5514b4;margin:0px auto;max-width:600px;">\n      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"\n      style="background:#5514b4;background-color:#5514b4;width:100%;">\n        <tbody>\n          <tr>\n            <td style="direction:ltr;padding:20px 0;text-align:center;">\n              <div class="mj-column-per-100 mj-outlook-group-fix" style="\n              text-align:left; direction:ltr;display:inline-block;vertical-align:top;width:100%;">\n\n          <table style="background-color: #5514B4; max-width: 600px;" width="100%"\n          role="presentation" cellspacing="0" cellpadding="0" border="0">\n          <tbody>\n          <tr>\n              <td width="2%" style="background-color: #5514B4;" class="footergutter">&nbsp;</td>\n              <td width="14%">\n                <a\n                ' + this.htmlAttributes({
        href: this.getAttribute('mybt-href')
      }) + '\n                style="color:#FFFFFF;font-size: 16px; line-height: 18px; font-family:\n                Century Gothic, CenturyGothic, CentGothWGL, -apple-system, BlinkMacSystemFont,\n                Arial, Tahoma, Geneva, sans-serif; font-weight: bold; text-decoration:none!important;"\n                class="footertext fallback-font">\n                MyBT\n                </a>\n              </td>\n              <td width="12%">\n                <a\n                ' + this.htmlAttributes({
        href: this.getAttribute('help-href')
      }) + '\n                style="color:#FFFFFF;font-size: 16px; line-height: 18px; font-family:\n                Century Gothic, CenturyGothic, CentGothWGL, -apple-system, BlinkMacSystemFont,\n                Arial, Tahoma, Geneva, sans-serif; font-weight: bold;text-decoration:none!important;"\n                class="footertext fallback-font">\n                Help</a>\n              </td>\n              <td width="14%" class="footerless">&nbsp;</td>\n              <td width="10%" align="right" class="footerwidthless">\n                <a\n                ' + this.htmlAttributes({
        href: this.getAttribute('twitter-href')
      }) + '\n                ><img src="twitter.gif" alt="BT Twitter" style="display: block;" height="25"\n                width="25" class="footericon"></a>\n              </td>\n              <td width="10%" align="right" class="footerwidthless">\n                <a\n                ' + this.htmlAttributes({
        href: this.getAttribute('facebook-href')
      }) + '\n                ><img src="facebook.gif" alt="BT Facebook" style="display: block;" height="25"\n                width="25" class="footericon"></a>\n              </td>\n              <td width="10%" align="right" class="footerwidthless" style="">\n                <a\n                ' + this.htmlAttributes({
        href: this.getAttribute('youtube-href')
      }) + '\n                ><img src="youtube.gif" alt="BT youtube" style="display: block;" height="25"\n                width="25" class="footericon"></a>\n              </td>\n              <td width="2%" style="background-color: #5514B4;" class="footergutter">&nbsp;</td>\n            </tr>\n            <tr>\n              <td colspan="8">&nbsp;</td>\n            </tr>\n            <tr>\n              <td width="2%" style="background-color: #5514B4;" class="footergutter">&nbsp;</td>\n              <td width="80%" colspan="6" class="footer-width">\n                <a\n                ' + this.htmlAttributes({
        href: this.getAttribute('unsubscribe-href')
      }) + '\n                 title="Unsubscribe" style="color:#FFFFFF;font-size: 16px; line-height: 18px;\n                 font-family: Century Gothic, CenturyGothic, AppleGothic Regular,\n                 Apple Gothic Regular, Arial, Tahoma, Geneva, sans-serif; font-weight: bold;\n                 text-decoration:none!important;" class="footertext fallback-font">\n                 Unsubscribe from BT Sport newsletters</a>\n                </td>\n              <td width="2%" style="background-color: #5514B4;" class="footergutter">&nbsp;</td>\n            </tr>\n          </tbody>\n          </table>\n        </td>\n      </tr>\n    </tbody>\n    </table>\n    </td>\n    </tr>\n    </table>\n    </div>\n    <!--[if mso | IE]>\n      </td>\n    </tr>\n  </table>\n  <![endif]-->\n\t\t';
    }

    // What the name suggests. Fallback value for this.getAttribute('attribute-name').

    // Tell the parser that our component won't contain other mjml tags

  }]);

  return MjBarFooterSport;
}(_mjmlCore.BodyComponent), _class.endingTag = false, _class.allowedAttributes = {
  'mybt-href': 'string',
  'help-href': 'string',
  'unsubscribe-href': 'string',
  'twitter-href': 'string',
  'facebook-href': 'string',
  'youtube-href': 'string' }, _class.defaultAttributes = {
  'mybt-href': 'null ',
  'help-href': 'null ',
  'unsubscribe-href': 'null ',
  'twitter-href': 'null ',
  'facebook-href': 'null ',
  'youtube-href': 'null ' }, _temp);
exports.default = MjBarFooterSport;